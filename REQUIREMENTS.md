# Todo Web Application

A Todo Web Application allows a user to create a list of Tasks to keep track of completion. This is done with a client application communicating with an API to create, update, and delete Tasks.

## Feature Requirements

* As a User, I want to be able to create a Task with a message to add to the list of Todo Tasks.
* As a User, I want to be able to mark a created Task as Completed.
* As a User, I want to be able to filter the list of Tasks by Completed, Not Completed, and All.
* As a User, I want the list of Tasks to be sorted by the time of creation.
* As a User, I want to be able to delete a Completed Task.
* As a User, I want to be able to update an existing Tasks message.
* As a User, I want client side validation during creating and updating a Task.

## Technical Requirements

* Client must be implemented using Blazor Web Assembly.
* Clients UI must be responsive for mobile and desktop devices.
* Server must be implemented in .NET Core.
* Data Access must use Entity Framework Core using a datastore that requires no manual installation of third party applications.
* A README.md file should be created for a developer to get the techinical requirements to run the application.

## Tasks
`You have 5 hours to complete all tasks.`

* Fork this repository.
* Implement the application with the stated requirements to your `master` branch.
* Share your code with `jonathan.dunn@paltronics.com.au` when completed.